import React, { useEffect, useRef, useState } from "react"
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { SafeAreaView, Text, ToastAndroid } from "react-native"
import AsyncStorage from '@react-native-community/async-storage'
import ReactNativePinView from "react-native-pin-view"
import { useNavigation } from '@react-navigation/native'
import { NAME_KEY_PIN } from '../config/config';
import { MESSAGES } from '../config/messages';

export default function SignIn() {
  const pinView = useRef(null);
  const [pin, setPin] = useState("");
  const [inputKey, setInputKey] = useState("");
  const [showCompletedButton, setShowCompletedButton] = useState(false);
  const navigation = useNavigation();


  useEffect(() => {
    getStoragePin();
  }, [])

  useEffect(() => {
    if (inputKey.length === 4) {
      if(inputKey === pin){        
        navigation.reset({index: 0, routes: [{ name: 'Devices' }]});
      }else{        
        ToastAndroid.showWithGravity(MESSAGES.SIGNIN_MESSAGE_ERROR, ToastAndroid.LONG, ToastAndroid.CENTER);
        pinView.current.clearAll();
      }

      setShowCompletedButton(true)
    } else {
      setShowCompletedButton(false)
    }
  }, [inputKey])


  const getStoragePin = async () => {
    try {
      const retrievedItem = await AsyncStorage.getItem(NAME_KEY_PIN);
      if (retrievedItem !== null) {
        setPin(retrievedItem);
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
        <SafeAreaView
          style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.7)", justifyContent: "center", alignItems: "center" }}>
          <Text
            style={{
              paddingTop: 0,
              paddingBottom: 0,
              color: "rgba(255,255,255,0.5)",
              fontSize: 20,
            }}>
            PIN:
          </Text>
          <ReactNativePinView
            inputSize={25}
            ref={pinView}
            pinLength={4}
            buttonSize={65}
            onValueChange={value => setInputKey(value)}
            buttonAreaStyle={{
              marginTop: 24,
            }}
            inputAreaStyle={{
              marginBottom: 24,
            }}
            inputViewEmptyStyle={{
              backgroundColor: "transparent",
              borderWidth: 1,
              borderColor: "#FFF",
            }}
            inputViewFilledStyle={{
              backgroundColor: "#FFF",
            }}
            buttonViewStyle={{
              borderWidth: 1,
              borderColor: "#FFF",
            }}
            buttonTextStyle={{
              color: "#FFF",
            }}
            onButtonPress={key => {
              if (key === "custom_left") {
                pinView.current.clear()
              }               
            }}
            customLeftButton={<Icon name={"backspace-outline"} size={36} color={"#FFF"} />}
            customRightButton={showCompletedButton ? <Icon name={"lock-open"} size={36} color={"#FFF"} /> : <Icon name={"lock-outline"} size={36} color={"#FFF"} />}
          />          
        </SafeAreaView>
  )
}