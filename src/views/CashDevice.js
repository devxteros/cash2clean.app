import React,{useState, useEffect} from "react";
import {View, Text, StyleSheet, ScrollView} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import init from 'react_native_mqtt';
import AsyncStorage from '@react-native-community/async-storage';
import { COLOR } from "../config/colors";
import { MESSAGES } from "../config/messages";


export default function CashDevice({navigation, route}) {

    global.mqttClient;
    const { serial } = route.params;
    const { name } = route.params;
    const [balance, setBalance] = useState({
        totalmonedas:"...",
        totalbilletes:"...",
        ultimobillete:"...",
        ultimomoneda:"...",
        billete1:"...",
        billete2:"...",
        billete5:"...",
        billete10:"...",
        billete20:"...",
        billete50:"...",
        billete100:"...",
        pantalla:"..."
    });    

    init({
        size: 10000,
        storageBackend: AsyncStorage,
        defaultExpires: 1000 * 3600 * 24,
        enableCache: true,
        reconnect: true,
        sync : {}
      });


    useEffect(()=>{
       global.mqttClient = new Paho.MQTT.Client('cash2clean.tk', 8080, "automatic213");
       global.mqttClient.onConnectionLost = onConnectionLost;
       global.mqttClient.onMessageArrived = onMessageArrived;
       global.mqttClient.connect({userName:'usrwemos', password:'dj22mes', onSuccess:onConnect });
    },[]);


    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:"+responseObject.errorMessage);
        }
    }

    function onMessageArrived(message) {
        renderData(message.payloadString);
    }

    function onConnect(){
        console.log("Conectado.");
        var message = new Paho.MQTT.Message("1");
        message.destinationName = "/cash2clean/output/"+serial;
        global.mqttClient.send(message);
        global.mqttClient.subscribe('/cash2clean/input/'+serial);
    }

    function renderData(payload){
        let new_balance = {
            totalmonedas:"?",
            totalbilletes:"?",
            ultimobillete:"?",
            ultimomoneda:"?",
            billete1:"?",
            billete2:"?",
            billete5:"?",
            billete10:"?",
            billete20:"?",
            billete50:"?",
            billete100:"?",
            pantalla:"?"
        };
        let array_grupo = payload.split('@');
        array_grupo.map((value, index)=>{
            let grupo = value.split('|');            
            switch (grupo[0]) {
                case "03":
                    if(grupo[1]=="1"){new_balance.billete1 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="2"){new_balance.billete2 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="5"){new_balance.billete5 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="10"){new_balance.billete10 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="20"){new_balance.billete20 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="50"){new_balance.billete50 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    if(grupo[1]=="100"){new_balance.billete100 = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                    break;
                case "04":
                    new_balance.totalmonedas = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    break;
                case "05":
                    new_balance.totalbilletes = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    break;
                case "06":
                    new_balance.pantalla = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    break;
                case "07":
                    new_balance.ultimobillete = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    break;
                case "08":
                    new_balance.ultimomoneda = grupo[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    break;
                default:
                    break;
            }
        });

        let result = setBalance(new_balance);
    }
    
    
    return(
        <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={styles.header}>BALANCE PARA {name}</Text>
            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Monedas</Text>
                    <Text style={styles.valor}>${balance.totalmonedas}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes</Text>
                    <Text style={styles.valor}>${balance.totalbilletes}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Última Transacción en Billete</Text>
                    <Text style={styles.valor}>${balance.ultimobillete}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Última Transacción en Monedas</Text>
                    <Text style={styles.valor}>${balance.ultimomoneda}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $1.000</Text>
                    <Text style={styles.valor}>${balance['billete1']}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $2.000</Text>
                    <Text style={styles.valor}>${balance.billete2}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $5.000</Text>
                    <Text style={styles.valor}>${balance.billete5}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $10.000</Text>
                    <Text style={styles.valor}>${balance.billete10}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $20.000</Text>
                    <Text style={styles.valor}>${balance.billete20}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $50.000</Text>
                    <Text style={styles.valor}>${balance.billete50}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Total Billetes $100.000</Text>
                    <Text style={styles.valor}>${balance.billete100}</Text>
                  </View>
              </View>
            </View>

            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'coin'} size={40} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.titulo}>Dinero en Pantalla</Text>
                    <Text style={styles.valor}>${balance.pantalla}</Text>
                  </View>
              </View>
            </View>
        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    taskbox: {
      margin: 1,
      backgroundColor: 'white',
      flexDirection: 'row',      
    },
    info: {
      flex: 4,
      alignItems: 'stretch',
      flexDirection: 'column',
      justifyContent: 'space-between',
      margin: 5
    },
    header:{
      fontSize: 22,
      color: '#550099',
      fontWeight: '700',
      margin:20
    },
    valor: {
      fontSize: 22,
      color: '#550099',
      fontWeight: '700',
    },
    titulo: {
      fontSize: 15,    
      color: '#666',
      fontWeight: '300',
    },
    iconContainer: {    
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#ffffff'
    },
  });