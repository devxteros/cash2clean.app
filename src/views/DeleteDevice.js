import React,{useState,useEffect} from "react";
import {StyleSheet, View, Text, TouchableOpacity, ToastAndroid} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { NAME_KEY_DEVICES } from '../config/config';
import { COLOR } from "../config/colors";
import {MESSAGES} from '../config/messages';


export default function DeleteDevice({navigation, route}) {

    const { key } = route.params;
    const { name } = route.params;
    const [devices, setDevices] = useState("");


    useEffect(()=>{        
        getStorageDevice();
    },[]);



    const getStorageDevice = async () => {
        try {
          const retrievedItem = await AsyncStorage.getItem(NAME_KEY_DEVICES);      
          if (retrievedItem !== null) {
            
            let array_device = retrievedItem.split("@");
            let state_device = [];
    
            array_device.map((item_device,index)=>{          
                let single_device = item_device.split("|");
                single_device = {key:index, name:single_device[0], location:single_device[1], serial:single_device[2]};
                state_device = state_device.concat(single_device);
            })
    
            setDevices(state_device);
            console.log(state_device);        
          }
        } catch (error) {
          console.log(error);
        }
      }


      function deleteStorageDevice(){
           let list_device = devices;
           list_device.splice(key, 1);           
           let new_str = [];

           list_device.map((value,index)=>{            
            new_str = new_str.concat(value.name + '|'+value.location + '|'+value.serial);
           });
           

           AsyncStorage.setItem(
                NAME_KEY_DEVICES,
                new_str.join("@"),
                () => {
                AsyncStorage.getItem(NAME_KEY_DEVICES, (err, result) => {             
                    ToastAndroid.showWithGravity(MESSAGES.DEVICE_DELETED, ToastAndroid.LONG, ToastAndroid.CENTER);
                    console.log(result);
                    navigation.goBack();
                });
                }
           );
      }




    return(
        <View style={{ flex: 1, flexDirection:'column', alignItems: 'center', justifyContent: 'center' }}>
            <Text style={styles.text}>{MESSAGES.DEVICE_ASK_DELETE+" "+name}?</Text>
            <View style={styles.container}>
            <TouchableOpacity   
               style={styles.submitButton}
               onPress = {()=>{deleteStorageDevice()}}>
               <Text style={styles.submitButtonText}>SI</Text>
            </TouchableOpacity>
            <TouchableOpacity   
               style={styles.submitButton}
               onPress = {()=>{navigation.goBack()}}>
               <Text style={styles.submitButtonText}>NO</Text>
            </TouchableOpacity>
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
       padding: 25,       
       justifyContent: 'space-around',       
       flexDirection: 'row',
    },
    submitButton: {
       marginTop: 50,
       backgroundColor: COLOR.SECONDARY_DARK,       
       padding: 25
    },
    submitButtonText:{
       color: 'white'
    },
    text:{
        fontSize:20
    }
 })