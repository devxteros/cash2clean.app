import React, {useState, useEffect} from 'react';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { NAME_KEY_PIN } from '../config/config';
import { COLOR } from '../config/colors';
import SignUp from './SignUp';
import SignIn from './SignIn';



function Main() {

  const [pin, setPin] = useState("");

  const getStorage = async (key) => {
    try {
      const retrievedItem = await AsyncStorage.getItem(key);
      if (retrievedItem !== null) {
        setPin(retrievedItem);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(()=>{
    getStorage(NAME_KEY_PIN);
  },[]);

  return pin ? <SignIn /> : <SignUp /> 
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',        
        justifyContent: 'center',
        alignItems: 'center'        
    },    
    red: {
      color: 'red',
    },
    text:{
        color: COLOR.WHITE,
        fontSize: 20,
    }
  });

export default Main;