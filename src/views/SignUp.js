import React, { useEffect, useRef, useState } from "react"
import { SafeAreaView, Text, ToastAndroid } from "react-native"
import { useNavigation } from '@react-navigation/native'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import AsyncStorage from '@react-native-community/async-storage'
import ReactNativePinView from "react-native-pin-view"
import {MESSAGES} from '../config/messages'
import { NAME_KEY_PIN } from '../config/config';

export default function SignUp() {
  const pinView = useRef(null)
  const [inputKey, setInputKey] = useState("");
  const [confirmKey, setConfirmKey] = useState("");  
  const [showCompletedButton, setShowCompletedButton] = useState(false);
  const navigation = useNavigation();

  useEffect(() => {
  
    if (inputKey.length === 4) {
      setShowCompletedButton(true)
    } else {
      setShowCompletedButton(false)
    }
    if((inputKey.length == 4) && (confirmKey == "")){
      setConfirmKey(inputKey);
      setInputKey("");
      pinView.current.clearAll();
    }
    if((inputKey.length == 4) && (confirmKey != "")){
      if(inputKey == confirmKey){
        savePin();
        alert(MESSAGES.SIGNUP_MESSAGE_SUCCESS);
        navigation.reset({index: 0, routes: [{ name: 'Devices' }]});
      }else{
        ToastAndroid.showWithGravity(MESSAGES.SIGNUP_INPUTKEY_NOCONFIRM, ToastAndroid.LONG, ToastAndroid.CENTER);
        pinView.current.clearAll();
        setInputKey("");
        setConfirmKey("");
      }
    }

  }, [inputKey]);

  savePin = async () => {
    try {
      await AsyncStorage.setItem(
        NAME_KEY_PIN,
        confirmKey
      );
    } catch (error) {
      ToastAndroid.showWithGravity(error, ToastAndroid.LONG, ToastAndroid.CENTER);
    }
  };

  return (
        <SafeAreaView
          style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.8)", justifyContent: "center", alignItems: "center" }}>
          <Text
            style={{
              paddingTop: 0,
              paddingBottom: 10,
              paddingLeft: 20,
              color: "rgba(255,255,255,0.5)",
              fontSize: 20,
            }}>
            {(confirmKey.length == "") ? MESSAGES.SIGNUP_INPUTKEY : MESSAGES.SIGNUP_INPUTKEY_CONFIRM}
          </Text>
          <ReactNativePinView
            inputSize={25}
            ref={pinView}
            pinLength={4}
            buttonSize={65}
            onValueChange={value => setInputKey(value)}
            buttonAreaStyle={{
              marginTop: 24,
            }}
            inputAreaStyle={{
              marginBottom: 24,
            }}
            inputViewEmptyStyle={{
              backgroundColor: "transparent",
              borderWidth: 1,
              borderColor: "#FFF",
            }}
            inputViewFilledStyle={{
              backgroundColor: "#FFF",
            }}
            buttonViewStyle={{
              borderWidth: 1,
              borderColor: "#FFF",
            }}
            buttonTextStyle={{
              color: "#FFF",
            }}
            onButtonPress={key => {
              if (key === "custom_left") {
                pinView.current.clear()
              }              
            }}
            customLeftButton={<Icon name={"backspace-outline"} size={36} color={"#FFF"} />}
            customRightButton={showCompletedButton ? <Icon name={"lock-open"} size={36} color={"#FFF"} /> : <Icon name={"lock-outline"} size={36} color={"#FFF"} />}
          />
        </SafeAreaView>
    
  )
}