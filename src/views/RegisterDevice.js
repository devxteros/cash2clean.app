import React, {useState, useEffect} from "react";
import {StyleSheet, View, TouchableOpacity, TextInput, Text, ToastAndroid} from "react-native";
import AsyncStorage from '@react-native-community/async-storage'
import { COLOR } from "../config/colors";
import {MESSAGES} from '../config/messages';
import { NAME_KEY_DEVICES } from '../config/config';


export default function SettingDevice({navigation}) {

    const [name, setName] = useState("");
    const [location, setLocation] = useState("");
    const [serial, setSerial] = useState("");
    const [devices, setDevices] = useState("");

   
    useEffect(()=>{       
       getStorageDevice();       
    },[]);



  const getStorageDevice = async () => {
   try {
     const retrievedItem = await AsyncStorage.getItem(NAME_KEY_DEVICES);
     if (retrievedItem !== null) {       
       setDevices(retrievedItem);
       console.log(retrievedItem);      
     }
   } catch (error) {
     console.log(error);
   }
 }


    function guardarDispositivo(){

      let device_object = name+"|"+location+"|"+serial;

      AsyncStorage.setItem(
         NAME_KEY_DEVICES,
         !devices ? device_object : devices+"@"+device_object,
         () => {
           AsyncStorage.getItem(NAME_KEY_DEVICES, (err, result) => {             
             ToastAndroid.showWithGravity(MESSAGES.REGISTER_DEVICE_SUCCESS, ToastAndroid.LONG, ToastAndroid.CENTER);
             navigation.goBack()
             console.log(result);
           });
         }
       );
    }

    return(
        <View style = {styles.container}>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {MESSAGES.REGISTER_DEVICE_NAME}
               placeholderTextColor = {COLOR.PRIMARY_LIGHT}
               autoCapitalize = "none"
               autoCompleteType = "off"
               maxLength={30}
               value={name}
               returnKeyType="next"
               onChangeText = {(name)=>{setName(name)}}/>
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {MESSAGES.REGISTER_DEVICE_LOCATION}
               placeholderTextColor = {COLOR.PRIMARY_LIGHT}
               autoCapitalize = "none"
               autoCompleteType = "off"
               maxLength={30}
               value={location}
               returnKeyType="next"
               onChangeText = {(location)=>{setLocation(location)}}/>

            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = {MESSAGES.REGISTER_DEVICE_SERIAL}
               placeholderTextColor = {COLOR.PRIMARY_LIGHT}
               autoCapitalize = "none"
               autoCompleteType = "off"
               maxLength={30}
               value={serial}
               returnKeyType="done"
               onChangeText = {(serial)=>{setSerial(serial)}}/>

            <TouchableOpacity
               style = {styles.submitButton}
               onPress = {()=>{guardarDispositivo()}}>
               <Text style={styles.submitButtonText}>Registrar</Text>
            </TouchableOpacity>
         </View>
    )
}

const styles = StyleSheet.create({
    container: {
       paddingTop: 25,
       paddingHorizontal: 20
    },    
    input: {
       margin: 10,
       height: 40,
       borderColor: COLOR.PRIMARY_DARK,
       borderWidth: 1
    },
    submitButton: {
       marginTop: 50,
       backgroundColor: COLOR.SECONDARY_DARK,
       alignItems: "center",
       padding: 10
    },
    submitButtonText:{
       color: 'white'
    }
 })