import React, {useState, useEffect} from "react";
import {StyleSheet, View, Text, TouchableOpacity, FlatList, TouchableHighlight, Alert} from "react-native";
import AsyncStorage from '@react-native-community/async-storage'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { NAME_KEY_DEVICES } from '../config/config';
import { COLOR } from "../config/colors";
import { MESSAGES } from "../config/messages";


export default function Devices({ navigation }) {

  const [devices, setDevices] = useState(null);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getStorageDevice();
      //console.log('Tengo foco....'+Date());
    });

    return unsubscribe;
  }, [navigation]);
  

  const getStorageDevice = async () => {
    try {
      const retrievedItem = await AsyncStorage.getItem(NAME_KEY_DEVICES);      
      if (retrievedItem !== null) {
        
        let array_device = retrievedItem.split("@");
        let state_device = [];

        array_device.map((item_device,index)=>{          
            let single_device = item_device.split("|");
            single_device = {key:index, name:single_device[0], location:single_device[1], serial:single_device[2]};
            state_device = state_device.concat(single_device);
        })

        setDevices(state_device);
      }else{
        setDevices(null);
      }
    } catch (error) {
      console.log(error);
    }
  }


    function itemComponent(item){      
        return(
            <TouchableHighlight onPress={()=>navigation.navigate('CashDevice', {"serial": item.serial, "name": item.name})}>
            <View style={styles.taskbox}>
              <View style={styles.iconContainer}>
                <Icon name={'cash-register'} size={60} color={COLOR.PRIMARY} />
              </View>
              <View style={styles.info}>
                  <View>
                    <Text style={styles.task}>{item.name}</Text>
                  </View>
                  <View>
                    <Text style={styles.customer}>{item.location}</Text>
                  </View>                  
                  <View>
                    <Text style={styles.status}>{item.serial}</Text>
                  </View>        
              </View>
                  <View>
                  <TouchableOpacity onPress={()=>navigation.navigate('DeleteDevice', {"key": item.key, "name": item.name})}>
                    <Icon name={'delete'} size={30} color={COLOR.GRAY} />
                  </TouchableOpacity>  
                  </View>    
            </View>
            </TouchableHighlight>
        )
    }

    if(devices != null){
      return(
        
              <View style={{ flex: 1, flexDirection: 'column' }}>              
                  <FlatList
                      data={devices}                    
                      renderItem={({item}) => itemComponent(item)}
                  />                
                  <TouchableOpacity onPress={()=>navigation.navigate('RegisterDevice')}
                  style={{
                      borderWidth:1,
                      borderColor:'rgba(0,0,0,0.3)',
                      alignItems:'center',
                      justifyContent:'center',
                      width:56,
                      height:56,
                      position: 'absolute',
                      bottom: 10,
                      right: 10,      
                      backgroundColor:COLOR.PRIMARY,
                      borderRadius:100,
                      }}>
                  <Icon name="plus"  size={30} color={COLOR.WHITE} />
                  </TouchableOpacity>             
              </View>
      )
  }else{
    return(
      <View style={{ flex: 1, flexDirection: 'column', alignItems:'center', justifyContent:'center'}}>
        <Text style={styles.comingsoon}>{MESSAGES.DEVICE_EMPTY}</Text>
        <TouchableOpacity onPress={()=>navigation.navigate('RegisterDevice', { names: ['Brent', 'Satya', 'Michaś'] })}
                  style={{
                      borderWidth:1,
                      borderColor:'rgba(0,0,0,0.3)',
                      alignItems:'center',
                      justifyContent:'center',
                      width:56,
                      height:56,
                      position: 'absolute',
                      bottom: 10,
                      right: 10,      
                      backgroundColor:COLOR.PRIMARY,
                      borderRadius:100,
                      }}>
                  <Icon name="plus"  size={30} color={COLOR.WHITE} />
                  </TouchableOpacity>  
      </View>
    )
  }
}

const styles = StyleSheet.create({
    taskbox: {
      margin: 1,
      backgroundColor: 'white',
      flexDirection: 'row',      
    },
    info: {
      flex: 4,
      alignItems: 'stretch',
      flexDirection: 'column',
      justifyContent: 'space-between',
      margin: 5
    },    
    customer: {
      fontSize: 12,    
      color: '#999',
    },
    task: {
      fontSize: 15,    
      color: '#222',
      fontWeight: '400',
    },
    status: {
      fontSize: 13,    
      color: '#777',
    },
    progress: {
      fontSize: 13,
      color: '#00FF00',
    },
    finalized: {
      fontSize: 13,
      color: '#FF0000',
    },
    comingsoon: {
      fontSize: 18,
      color: '#777',
    },
    iconContainer: {    
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#fff'
    },
  });