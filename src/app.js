import React from 'react';
import { Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CashDevice from './views/CashDevice';
import Devices from './views/Devices';
import RegisterDevice from './views/RegisterDevice';
import DeleteDevice from './views/DeleteDevice';
import Main from './views/Main';

const Stack = createStackNavigator();

function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main">
        <Stack.Screen name="Main" component={Main} options={{headerShown: false}} />
        <Stack.Screen name="Devices" component={Devices} options={{ title: 'Dispositivos' }} />
        <Stack.Screen name="RegisterDevice" component={RegisterDevice} options={{ title: 'Agregar Dispositivo' }} />
        <Stack.Screen name="CashDevice" component={CashDevice} options={{ title: 'Dinero en Caja' }} />
        <Stack.Screen name="DeleteDevice" component={DeleteDevice} options={{ title: 'Borrar Dispositivo' }} />        
      </Stack.Navigator>
    </NavigationContainer>
  );
}
// options={{title: 'Registro', headerTitle: props => <LogoTitle {...props} />}}

function LogoTitle() {
    return (
      <Image
        style={{ width: 125, height: 20 }}
        source={require('./assets/logo.png')}
      />
    );
}



export default App;