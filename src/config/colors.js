/* 
Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences.
COLOR TOOL
Create, share, and apply color palettes to your UI, as well as measure the accessibility level of any color combination.
Visit: https://material.io/color
*/
export const COLOR = {
	PRIMARY:'#2196f3',
	PRIMARY_LIGHT:'#2a338f',
	PRIMARY_DARK:'#2a338f',
	SECONDARY:'#42b3e3',
	SECONDARY_LIGHT:'#5eb8ff',
	SECONDARY_DARK:'#005b9f',
	TEXT_PRIMARY:'#ffffff',
	TEXT_SECONDARY:'#ffffff',
	BLACK:'#000000',
	WHITE:'#ffffff',
	GRAY:'#999'
}