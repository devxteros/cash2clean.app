export const LIST_EVENT_STATUS = {
	IN_PROGRESS: 'EN CURSO',
	COMING_SOON: 'PROXIMAMENTE',
	FINALIZED: 'FINALIZADO'
};

export const DEVICE_TYPE = {
	ICON:[
		'',
		'lightbulb-on-outline',
		'power-plug',
		'video',
		'air-conditioner',
		'thermometer-lines',
		'glassdoor'
	]	
}