export const MESSAGES = {
	SIGNUP_INPUTKEY: 'Registra un PIN de 4 digitos para ingresar al sistema.',
    SIGNUP_INPUTKEY_CONFIRM: 'Digita nuevamente para confirmar tu PIN.',
    SIGNUP_INPUTKEY_NOCONFIRM: 'La confirmacion no coincide con tu PIN. Por favor intenta de nuevo.',
    SIGNUP_MESSAGE_SUCCESS: 'Tu PIN se registró con exito. Debes usarlo la próxima vez que inicies la aplicación.',
    SIGNIN_MESSAGE_ERROR: 'PIN inválido, Intentalo de nuevo.',    
    REGISTER_DEVICE_BUTTON: 'REGISTRAR DISPOSITIVO',
    REGISTER_DEVICE_NAME: 'Nombre',
    REGISTER_DEVICE_LOCATION: 'Ubicación',
    REGISTER_DEVICE_SERIAL: 'Número Serial',
    REGISTER_DEVICE_SUCCESS: 'El dispositivo se registró con éxito.',
    DEVICE_DELETED:'El dispositivo se eliminó de la lista.',
    DEVICE_EMPTY:'No hay dispositivos asociados.',
    DEVICE_ASK_DELETE:'Borrar dispositivo',

}